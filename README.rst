=======================================
AWS Caltech.edu Redirector EC2 Instance
=======================================

The sole purpose of this EC2 instanace is to redirect https://caltech.edu 
to https://www.caltech.edu. That is all this instance does.

It was created prior to AWS supporting a static or elastic IP to be assigned
to an AWS load balancer. We'll likely implement that option, eventually.

==
Dan Caballero
