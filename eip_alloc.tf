# =================================================================== #
# Allocating existing EIP 52.26.125.194 to Caltech.edu Redirect Host  #
# =================================================================== #
resource "aws_eip_association" "caltech-edu-redirect-eip-assoc" {
  instance_id = "${aws_instance.caltech-edu-redirect.id}"
  allocation_id = "eipalloc-2039ed1d"
}
