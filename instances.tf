# ==================================================
#
# EC2 Instances
#
# ==================================================

# Defining caltech-edu-redirect host

resource "aws_instance" "caltech-edu-redirect" {
  instance_type			= "${var.caltech-edu-redirect_host_instance_type}"
  ami				= "${var.caltech-edu-redirect_host_ami}"
  key_name			= "${var.aws_key_name}"
  vpc_security_group_ids	= ["sg-c466e3bc","sg-c566e3bd","sg-b966e3c1","sg-fd9c4787"]
  subnet_id			= "subnet-ec12b48b"
  iam_instance_profile		= "${var.run_command_role}"
  root_block_device {
    volume_type 		= "gp2"
    delete_on_termination 	= true
  }

  tags {
    Name 			= "Caltech.edu Redirect Host"
    Environment 		= "${var.tag_environment}"
    Project     		= "${var.tag_project}"
    Group       		= "${var.tag_group}"
    Client      		= "${var.tag_client}"
    Contact     		= "${var.tag_contact}"
  }

   provisioner "file" {
    connection {
      user 			= "${var.provisioner_user}"
      host 			= "${aws_instance.caltech-edu-redirect.public_ip}"
      timeout 			= "${var.provisioner_timeout}"
      private_key		= "${file("${var.key_location}")}"
    }
    source			= "provision/launch_script.sh"
    destination			= "/tmp/launch_script.sh"
  }

 provisioner "file" {
    connection {
      user 			= "${var.provisioner_user}"
      host 			= "${aws_instance.caltech-edu-redirect.public_ip}"
      timeout 			= "${var.provisioner_timeout}"
      private_key		= "${file("${var.key_location}")}"
    }
    source			= "provision/web_caltech_edu.crt"
    destination			= "/home/ec2-user/web_caltech_edu.crt"
  }

 provisioner "file" {
    connection {
      user 			= "${var.provisioner_user}"
      host 			= "${aws_instance.caltech-edu-redirect.public_ip}"
      timeout 			= "${var.provisioner_timeout}"
      private_key		= "${file("${var.key_location}")}"
    }
    source			= "provision/web.caltech.edu.key"
    destination			= "/home/ec2-user/web.caltech.edu.key"
  }

 provisioner "file" {
    connection {
      user 			= "${var.provisioner_user}"
      host 			= "${aws_instance.caltech-edu-redirect.public_ip}"
      timeout 			= "${var.provisioner_timeout}"
      private_key		= "${file("${var.key_location}")}"
    }
    source			= "provision/caltech.edu.conf"
    destination			= "/home/ec2-user/caltech.edu.conf"
  }

 provisioner "file" {
    connection {
      user 			= "${var.provisioner_user}"
      host 			= "${aws_instance.caltech-edu-redirect.public_ip}"
      timeout 			= "${var.provisioner_timeout}"
      private_key		= "${file("${var.key_location}")}"
    }
    source			= "provision/acs-InCommonRSA.pem"
    destination			= "/home/ec2-user/acs-InCommonRSA.pem"
  }

  provisioner "remote-exec" {
    connection {
      user 			= "${var.provisioner_user}"
      host			= "${aws_instance.caltech-edu-redirect.public_ip}"
      timeout			= "${var.provisioner_timeout}"
      private_key		= "${file("${var.key_location}")}"
    }
    inline = [
      "sudo chmod +x /tmp/launch_script.sh",
      "sudo /tmp/launch_script.sh",
      "sudo sysctl kernel.hostname=caltech.edu",
      "sudo sed -i 's/localhost.localdomain/caltech.edu/g' /etc/sysconfig/network",
    ]
  }
}
