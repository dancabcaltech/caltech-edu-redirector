# ==================================================
#
# AWS environment
#
# ==================================================

# Defining provider and aws cli profile to use

provider "aws" {
  region    = "${var.aws_region}" 
  profile   = "${var.aws_profile}"
}
