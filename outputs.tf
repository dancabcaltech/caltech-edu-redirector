# ==================================================
#
# Variables to Output
#
# ==================================================

output "caltech-edu-redirect-public-ip" {
  value = "${aws_eip_association.caltech-edu-redirect-eip-assoc.public_ip}"
}

