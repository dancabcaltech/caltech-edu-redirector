#!/bin/bash

#Register with Spacewalk
echo "192.168.19.41 spacewalk.caltech.edu spacewalk" >> /etc/hosts
sudo cp -fv /home/ec2-user/rh-config.sh /root/.
sudo chmod +x /root/rh-config.sh && /root/./rh-config.sh
sudo cp -fv /home/ec2-user/gpg-key-import.sh /root/.
sudo chmod +x /root/gpg-key-import.sh && /root/./gpg-key-import.sh

# installing packages
sudo yum install -y nfs-utils httpd mod_ssl sssd openldap-clients wget openssl-devel pam-devel firewalld pam_ldap git rsyslog-gnutls
sudo yum update -y

# Define mount

sudo mkdir /mnt/efs
EFS_FILE_SYSTEM_ID="fs-2505c98c"
DIR_SRC="$EFS_FILE_SYSTEM_ID.efs.us-west-2.amazonaws.com:/"
DIR_TGT=/mnt/efs
touch /home/ec2-user/echo.txt
echo $EFS_FILE_SYSTEM_ID >> /home/ec2-user/echo.txt
echo $EC2_AVAIL_ZONE >> /home/ec2-user/echo.txt
echo "us-west-2" >> /home/ec2-user/echo.txt
echo $DIR_SRC >> /home/ec2-user/echo.txt
echo $DIR_TGT >> /home/ec2-user/echo.txt

sudo mount -t nfs4 $DIR_SRC $DIR_TGT >> /home/ec2-user/echo.txt
echo $DIR_SRC $DIR_TGT nfs4 defaults,nofail 0 0 | sudo tee --append /etc/fstab
sudo mount -a

#Firewalld configuration
#sudo cp -fv /mnt/efs/acs-configs/httpd-public.xml /etc/firewalld/zones/public.xml
#sudo firewall-cmd --reload

# symlinks for login shells
sudo ln -s /bin/bash /usr/local/bin/bash
sudo ln -s /bin/csh /usr/local/bin/csh
sudo ln -s /bin/tcsh /usr/local/bin/tcsh
sudo ln -s /bin/zsh /usr/local/bin/zsh

# copying system configs into place
sudo cp -fv /mnt/efs/acs-configs/nsswitch.conf /etc/nsswitch.conf
sudo cp -fv /mnt/efs/acs-configs/ldap.conf /etc/openldap/ldap.conf
sudo cp -fv /mnt/efs/acs-configs/sssd.conf /etc/sssd/sssd.conf
sudo cp -fv /mnt/efs/acs-configs/pam_ldap.conf /etc/pam_ldap.conf
sudo cp -fv /mnt/efs/acs-configs/password-auth-ac /etc/pam.d/password-auth-ac
sudo cp -fv /mnt/efs/acs-configs/pam-duo-sudo /etc/pam.d/sudo
sudo cp -fv /mnt/efs/acs-configs/system-auth-ac /etc/pam.d/system-auth-ac
sudo cp -fv /mnt/efs/acs-configs/hosts /etc/hosts
sudo cp -fv /mnt/efs/acs-configs/sudoers /etc/sudoers
sudo cp -fv /mnt/efs/acs-configs/access.conf /etc/security/access.conf
sudo cp -fv /mnt/efs/acs-configs/rsyslog.conf /etc/rsyslog.conf
sudo cp -fv /mnt/efs/acs-configs/beats.repo /etc/yum.repos.d/beats.repo

# install SSL key and certificate
sudo move /home/ec2-user/web.caltech.edu.key /etc/pki/tls/private/.
sudo move /home/ec2-user/web_caltech_edu.crt /etc/pki/tls/certs/.
sudo move /home/ec2-user/acs-InCommonRSA.pem /etc/pki/tls/certs/.
sudo chown root:root /etc/pki/tls/private/*
sudo chown root:root /etc/pki/tls/certs/*

# steps for configuring HTTPD
sudo setenforce permissive
sudo mv /home/ec2-user/caltech.edu.conf /etc/httpd/conf.d/.
sudo chown root:root /etc/httpd/conf.d/*.conf
sudo systemctl enable httpd
sudo systemctl start httpd

# install duo and config pam
sudo cp -fv /mnt/efs/acs-configs/pam-sudo /etc/pam.d/sudo
cd /home/ec2-user/
wget https://dl.duosecurity.com/duo_unix-latest.tar.gz
tar zxf duo_unix-latest.tar.gz
cd duo_unix-1.10.1
sudo ./configure --with-pam --prefix=/usr && make && sudo make install
sudo cp -fv /mnt/efs/acs-configs/pam_duo.conf /etc/duo/pam_duo.conf

# setting up beats repo for fluentd logging
sudo yum install -y filebeat
sudo cp -fv /mnt/efs/acs-configs/filebeat.yml /etc/filebeat/filebeat.yml
sudo cp -fv /mnt/efs/acs-configs/td-agent.conf /tmp/td-agent.conf
sudo rpm --import "http://packages.elastic.co/GPG-KEY-elasticsearch"
sudo chkconfig filebeat on
sudo systemctl start filebeat
sudo curl -L "https://toolbelt.treasuredata.com/sh/install-redhat-td-agent2.sh" | sh
sudo mv /tmp/td-agent.conf /etc/td-agent/td-agent.conf
sudo chkconfig td-agent on
sudo /etc/init.d/td-agent start

# setting timezone and clock

sudo sed -i s/UTC/America\/Los\_Angeles/g /etc/sysconfig/clock
sudo ln -sf /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

sudo sed -i s/PasswordAuthentication\ no/PasswordAuthentication\ yes/g /etc/ssh/sshd_config
sudo systemctl restart sshd
sudo chkconfig sssd on
sudo systemctl start sssd

sudo systemctl restart rsyslog

sudo authconfig --enablemkhomedir --update

#Run Command
sudo mkdir -p /tmp/ssm
sudo curl https://amazon-ssm-us-west-2.s3.amazonaws.com/latest/linux_amd64/amazon-ssm-agent.rpm -o /tmp/ssm/amazon-ssm-agent.rpm
sudo yum install -y /tmp/ssm/amazon-ssm-agent.rpm
sudo start amazon-ssm-agent

exit 0
