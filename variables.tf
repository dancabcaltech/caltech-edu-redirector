# ==================================================
#
# Terraform Variables
#
# ==================================================

# ===========================
# Tagging
# ===========================

variable "tag_environment" {
  default = "Prod"
}

variable "tag_project" {
  default = "Caltech.edu Redirect"
}

variable "tag_group" {
  default = "ACS"
}

variable "tag_client" {
  default = "ACS"
}

variable "tag_contact" {
  default = "Dan Caballero"
}

# ===========================
# 1. main.tf
# ===========================

# Defining AWS region

variable "aws_region" {
  default = "us-west-2"
}

# Defining AWS profile

variable "aws_profile" {
  default = "default"
}

# Key used to launch instances

variable "aws_key_name" {
  default = "caltech-edu-redirect"
}

# key location

variable "key_location" {
  default = "/Users/dancab/caltech-edu-redirect.pem"
}

# ARN of IAM Role for run command

variable "run_command_role" {
  default = "run-command-role"
}

# ===========================
# 2. vpc.tf
# ===========================

# ===========================
# 3. security_groups.tf
# ===========================

# ===========================
# 4. load_balancers.tf
# ===========================

# ===========================
# 5. instances.tf
# ===========================
# Variable defining instance type

variable "caltech-edu-redirect_host_instance_type" {
  default = "t2.large"
}

# ami to launch instance

variable "caltech-edu-redirect_host_ami" {
  default = "ami-3d726944"
}
  
# Provisioner user

variable "provisioner_user" {
  default = "ec2-user"
}

# Provisioner connection timeout value

variable "provisioner_timeout" {
  default = "1m"
}

# ===========================
# 7. efs.tf
# ===========================
